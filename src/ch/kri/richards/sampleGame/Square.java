package ch.kri.richards.sampleGame;

import java.awt.Color;

import ch.kri.richards.gameObjects.AnimObjectMobile;
import ch.kri.richards.gameObjects.AnimationObject;
import ch.kri.richards.gameObjects.CollisionEvent;
import ch.kri.richards.gameObjects.CollisionListener;

public class Square extends AnimObjectMobile {

	public Square(String name, int width, int height, int xPos, int yPos, boolean bounce, Color c) {
		super(name, width, height, xPos, yPos, 1, bounce, c);
	}
	
}
