package ch.kri.richards.sampleGame;

import java.awt.Color;
import java.awt.Graphics;

import ch.kri.richards.gameObjects.AnimationObject;
import ch.kri.richards.gameObjects.CollisionEvent;
import ch.kri.richards.gameObjects.CollisionListener;

public class Target extends AnimationObject {
	private int changeColor = 0;
	
	public Target(String name, int width, int height, int xPos, int yPos, Color c) {
		super(name, width, height, xPos, yPos, c);
	}

	@Override
	public void paint(Graphics g) {
		if (changeColor > 0) {
			g.setColor(Color.magenta);
			changeColor--;
		} else {
			g.setColor(color);
		}
		g.fillRect((int) position.x, (int) position.y, width, height);
	}

	@Override
	protected void collision(AnimationObject o) {
		// Destroy whatever hit us
		o.destroy();
		changeColor = 10;
	}

}
