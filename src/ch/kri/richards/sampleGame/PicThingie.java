package ch.kri.richards.sampleGame;

import java.awt.Image;

import ch.kri.richards.gameObjects.AnimObjectMobile;

public class PicThingie extends AnimObjectMobile {

	public PicThingie(String name, int width, int height, int xPos, int yPos, boolean bounce, String imageName) {
		super(name, width, height, xPos, yPos, 1, bounce, imageName);
	}

}
