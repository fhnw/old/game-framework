package ch.kri.richards.sampleGame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import ch.kri.richards.gameObjects.AnimObjectMobile;
import ch.kri.richards.gameObjects.AnimationObject;
import ch.kri.richards.gameObjects.CollisionEvent;
import ch.kri.richards.gameObjects.CollisionListener;
import ch.kri.richards.gameObjects.Vector;

public class Ball extends AnimObjectMobile implements KeyListener {
	private int thickness;

	public Ball(String name, int width, int height, int xPos, int yPos, boolean bounce, Color c) {
		super(name, width, height, xPos, yPos, 5, bounce, c);
		thickness = 2;
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(color);
		Stroke oldStroke = g2.getStroke();
		g2.setStroke(new BasicStroke(thickness));
		g.drawOval((int) position.x, (int) position.y, width, height);
		g2.setStroke(oldStroke);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		switch (arg0.getKeyChar()) {
		case 'a': case 'A':
			this.velocity = this.velocity.subtract(new Vector(1,0));
			break;
		case 's': case 'S':
			this.velocity = this.velocity.add(new Vector(0,1));
			break;
		case 'd': case 'D':
			this.velocity = this.velocity.add(new Vector(1,0));
			break;
		case 'w': case 'W':
			this.velocity = this.velocity.subtract(new Vector(0,1));
			break;
		}
	}
}
