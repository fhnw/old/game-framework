package ch.kri.richards.sampleGame;

import java.awt.Color;
import java.awt.Graphics;

import ch.kri.richards.gameObjects.AnimationObject;

public class Block extends AnimationObject {
	public Block(String name, int width, int height, int xPos, int yPos, Color c) {
		super(name, width, height, xPos, yPos, c);
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(color);
		g.fillRect((int) position.x, (int) position.y, width, height);
	}

}
