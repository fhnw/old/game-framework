package ch.kri.richards.sampleGame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.JLabel;
import javax.swing.JTextField;

import ch.kri.richards.gameObjects.GameTemplate;

public class Bouncy extends GameTemplate {
	private Ball b; // the object that will get keyboard input;
	
	public Bouncy() {
		super();
		
		North_behavior = WallBehavior.Wrap;
		South_behavior = WallBehavior.Bounce;
		East_behavior = WallBehavior.Bounce;
		West_behavior = WallBehavior.Bounce;
		
		// The objects to herd
		for (int i = 0; i < 10; i++) {
			Square o = new Square(("o" + Integer.toString(i)), 10, 10, (int) (400 * Math.random()),
					(int) (400 * Math.random()), true, Color.blue);
			o.setVelocity((float) (30 * Math.random()), (float) (30 * Math.random()));
			addObject(o);
		}

		// Also an image
		PicThingie p = new PicThingie("ship", 20, 20, 100, 100, true, "/ch/kri/richards/images/ship.gif");
		p.setVelocity(-10, -10);
		addObject(p);		
		
		// Two obstacles in the middle
		Block obstacle = new Block("horiz", 80, 20, 160, 190, Color.blue);
		addObject(obstacle);
		obstacle = new Block("vert", 20, 80, 190, 160, Color.blue);
		addObject(obstacle);
		
		// Target at the top
		Target t = new Target("target", 80, 10, 160, 0, Color.red);
		addObject(t);

		// The ball gets keyboard events
		b = new Ball(("b" + Integer.toString(1)), 20, 20, (int) (400 * Math.random()),
				(int) (400 * Math.random()), false, Color.red);
		b.setVelocity((float) (30 * Math.random()), (float) (30 * Math.random()));
		b.setAcceleration((float) 0.0, (float) 10.0);
		addObject(b);
	}
	
	@Override
	public void init() {
		super.init();
		
		// This is where you can set backgrouns and such for the game area
		this.gameArea.setBackground(Color.GREEN);
		this.gameArea.setBackgroundImage("/ch/kri/richards/images/Polo.gif");
		
		// This is where you can add your own controls to the Applet - just leave the Center alone!
		this.getContentPane().add(new JTextField("Hello, World!"), BorderLayout.NORTH);
		this.gameArea.addKeyListener(b);
	}
	
	
}
