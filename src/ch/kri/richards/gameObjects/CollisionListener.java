package ch.kri.richards.gameObjects;

import java.util.EventListener;

public interface CollisionListener extends EventListener {
  public void collisionEvent(CollisionEvent e);
}
