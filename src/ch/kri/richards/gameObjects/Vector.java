package ch.kri.richards.gameObjects;

/**
 * Vector holds values representing a vector in Euclidean 2-space. For convenience, this class
 * exposes the Vector components as public, mutable fields.
 * 
 * This class has been modified to be more object-oriented. Calculation methods return new vectors,
 * rather than modifying the existing one, except explicit "set" methods. This makes the class less
 * efficient, but more understandable.
 * 
 * @author John B. Matthews, modified by Brad Richards
 */
public class Vector {

	/** The Vector's x component. */
	public float x;
	/** The Vector's y component. */
	public float y;

	/** Construct a null Vector, <0, 0> by default. */
	public Vector() {
	}

	/**
	 * Construct a new Vector from two doubles.
	 * 
	 * @param x
	 *          the x component
	 * @param y
	 *          the y component
	 */
	public Vector(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Construct a new Vector from two integers.
	 * 
	 * @param x
	 *          - the x component
	 * @param y
	 *          - the y component
	 */
	public Vector(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Construct a new Vector from an existing one.
	 * 
	 * @param v
	 *          - the source Vector
	 */

	public Vector(Vector v) {
		this.x = v.x;
		this.y = v.y;
	}

	/**
	 * Set the components of this Vector.
	 * 
	 * @param x
	 *          - the x component
	 * @param y
	 *          - the y component
	 * @return this Vector.
	 */
	public Vector set(float x, float y) {
		this.x = x;
		this.y = y;
		return this;
	}

	/**
	 * Set the components of this Vector to those of v.
	 * 
	 * @param v
	 *          - the source Vector
	 * @return this Vector.
	 */
	public Vector set(Vector v) {
		this.x = v.x;
		this.y = v.y;
		return this;
	}

	/**
	 * Return the scalar norm (length) of this Vector.
	 * 
	 * @return the norm of this Vector
	 */
	public float norm() {
		return (float) Math.hypot(x, y);
	}

	/**
	 * Add the given Vector to this Vector; return the result as a new Vector.
	 * 
	 * @param v
	 *          - the given Vector
	 * @return the sum
	 */
	public Vector add(Vector v) {
		return new Vector(x + v.x, y + v.y);
	}

	/**
	 * Subtract the given Vector from this Vector; return the result as a new Vector.
	 * 
	 * @param v
	 *          - the given Vector
	 * @return the difference
	 */
	public Vector subtract(Vector v) {
		return new Vector(x - v.x, y - v.y);
	}

	/**
	 * Multiply the given Vector by this Vector; return the scalar product.
	 * 
	 * @param v
	 *          - the given Vector
	 * @return the scalar (dot) product
	 */
	public float dot(Vector v) {
		return (x * v.x) + (y * v.y);
	}

	/**
	 * Scale this Vector by the given scale factor; return the result as a new vector
	 * 
	 * @param s
	 *          the scale factor
	 * @return the this Vector, scaled by s
	 */
	public Vector scale(float s) {
		return new Vector(x * s, y * s);
	}

	/**
	 * Scale this Vector by 1 / norm(); return this as a new Vector. The result is a unit Vector parallel to
	 * the original. This is equivalent to this.scale(1 / this.norm()), with a check for division by
	 * zero.
	 * 
	 * @return the this Vector, scaled to unit length
	 */
	public Vector unitVector() {
		Vector v = null;
		float d = norm();
		if (d != 0) {
			v = new Vector(x / d, y / d);
		} else {
			// We must have some default
			v = new Vector(1,0);
		}
		return v;
	}

	/** Return this Vector's String representation. */
	public String toString() {
		return "<" + this.x + ", " + this.y + ">";
	}
}