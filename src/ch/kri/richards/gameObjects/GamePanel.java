package ch.kri.richards.gameObjects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class GamePanel extends JPanel {
	private GameTemplate parent;
	private int width;
	private int height;
	private Image backgroundImage;

	private BufferedImage buff; // Buffer for off-screen image
	private Graphics gBuff; // Graphics object for buffer

	private Border focusBorder = new LineBorder(Color.black, 1);

	public GamePanel(GameTemplate parent) {
		super();
		this.parent = parent;
		this.width = width;
		this.height = height;
		this.setFocusable(true);
		this.setBorder(focusBorder);
	}
	
	public void setBackgroundImage(String imageName) {
		URL url = this.getClass().getResource(imageName);
		Toolkit t = Toolkit.getDefaultToolkit();
		Image i = t.createImage(url);
		setBackgroundImage(i);
	}
	
	public void setBackgroundImage(Image image) {
		this.backgroundImage = image;
	}

	@Override
	public void paint(Graphics pGraphics) {
		// Create the buffer here; once the Applet is displayed, our size is set
		if (buff == null) {
			width = this.getWidth();
			height = this.getHeight();
			buff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			gBuff = buff.createGraphics();
		}

		gBuff.setColor(this.getBackground());
		gBuff.fillRect(0, 0, width + 1, height + 1);
		if (backgroundImage != null) gBuff.drawImage(backgroundImage, 0, 0, this.width, this.height, this.getBackground(), null);
		ArrayList<AnimationObject> gameObjects = parent.getGameObjects();
		synchronized (gameObjects) {
			for (AnimationObject thing : gameObjects) {
				thing.paint(gBuff);
			}
		}
		if (this.hasFocus()) {
			gBuff.setColor(Color.black);
			gBuff.drawLine(0, 0, width - 1, 0);
			gBuff.drawLine(width - 1, 0, width - 1, height - 1);
			gBuff.drawLine(width - 1, height - 1, 0, height - 1);
			gBuff.drawLine(0, height - 1, 0, 0);
		}
		pGraphics.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), parent);
	}
}
