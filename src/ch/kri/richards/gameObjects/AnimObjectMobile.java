package ch.kri.richards.gameObjects;

import java.awt.Color;
import java.awt.Image;

/**
 * This class models movable objects. Regardless of the actual presentation, these objects are
 * treated as billiard balls, i.e., collisions should based on ideal 2-dimensional elastic
 * collisions of round objects.
 * 
 * If the object is set to not bounce, this means that it will overrun other objects. The other object will 
 * bounce normally, unless it is also set to not bounce (this would be strange behavior).
 * 
 * @author brad
 * 
 */
public abstract class AnimObjectMobile extends AnimationObject {
	protected Vector acceleration = new Vector(); // acceleration as a vector, in pixels per second, default 0
	protected Vector velocity = new Vector(); // velocity as a vector, in pixels per second, default 0
	protected float radius;
	protected float mass;
	protected boolean bounce;

	public AnimObjectMobile(String name, int width, int height, int x_pos, int y_pos, float mass, boolean bounce,
			Color c) {
		super(name, width, height, x_pos, y_pos, c);
		radius = ((float) width + height) / 4;
		this.mass = mass;
		this.bounce = bounce;
	}

	public AnimObjectMobile(String name, int width, int height, int x_pos, int y_pos, float mass, boolean bounce,
			String imageName) {
		super(name, width, height, x_pos, y_pos, imageName);
		radius = ((float) width + height) / 4;
		this.mass = mass;
		this.bounce = bounce;
	}

	public void setAcceleration(float x_acc, float y_acc) {
		acceleration.set(x_acc, y_acc);
	}

	public void setVelocity(float x_vel, float y_vel) {
		velocity.set(x_vel, y_vel);
	}
	
	/**
	 * Called by the GameTemplate to move the object. The game template handles simple movement; this
	 * is an opportunity for the object to handle any special aspects of movement, such as non-linear
	 * acceleration.
	 * 
	 * @param ms
	 *          - time elapsed in milliseconds since this object last moved
	 */
	public void move(long ms) {
	}
}
