package ch.kri.richards.gameObjects;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.swing.JApplet;
import javax.swing.JPanel;

/**
 * Copyright 2011 Brad Richards (http://richards.kri.ch/)
 * 
 * This class implements the basic functionality required for a simple game
 * running as an applet.
 * 
 * This is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This software is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 */
public abstract class GameTemplate extends JApplet implements Runnable {
	private int yMax; // Size of the paying field
	private int xMax; // Size of the paying field
	private Thread displayThread;
	private volatile boolean stopThread; // Used to stop the repaint thread

	// Wall behavior: for each wall, true indicates that the object bounces,
	// false indicates that it is destroyed when passing the wall coordinates
	protected WallBehavior North_behavior = WallBehavior.Bounce;
	protected WallBehavior East_behavior = WallBehavior.Bounce;
	protected WallBehavior West_behavior = WallBehavior.Bounce;
	protected WallBehavior South_behavior = WallBehavior.Bounce;

	// The game area
	protected GamePanel gameArea;

	// For info - frames per second
	protected long frames;
	private int FPS;

	// All objects in the game are registered in this ArrayList
	private ArrayList<AnimationObject> gameObjects = new ArrayList<AnimationObject>();

	@Override
	public void init() {
		// Set up GUI: JPanel in the Center, other components can be added by the
		// subclass...
		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		gameArea = new GamePanel(this);
		gameArea.setBackground(Color.WHITE);
		contentPane.add(gameArea, BorderLayout.CENTER);
		gameArea.requestFocus();

	}

	@Override
	public void start() {
		// When the Applet is displayed, determine the actual width and height of
		// the playing field
		xMax = gameArea.getWidth();
		yMax = gameArea.getHeight();

		// Set up the display thread
		stopThread = false;
		displayThread = new Thread(this);
		displayThread.setName("Game-Repaint");
		displayThread.start();
	}

	@Override
	public void stop() {
		stopThread = true;
	}

	@Override
	public void run() {
		// For the first execution, we assume a frame-rate of 50FPS
		long thisTime = (new Date()).getTime() - 20;
		long lastTime, elapsedTime;
		frames = 0;
		long totalTime = 0;

		// Only start once the Applet has been displayed

		while (!stopThread) { // continue until we are told to stop
			lastTime = thisTime;
			thisTime = (new Date()).getTime();
			elapsedTime = thisTime - lastTime;

			// FPS math
			frames++;
			totalTime += elapsedTime;
			if ((frames % 100) == 0 & totalTime != 0) {
				FPS = (int) (100000 / totalTime);
				totalTime = 0;
			}

			// Move all objects
			synchronized (gameObjects) {
				for (Iterator<AnimationObject> i = gameObjects.iterator(); i.hasNext();) {
					AnimationObject thing = i.next();
					
					// Give objects a chance to do something each frame
					thing.doEachFrame();
					
					// Only move things that are mobile
					if (thing instanceof AnimObjectMobile) {
						AnimObjectMobile om = (AnimObjectMobile) thing;
						float timeScaleFactor = (float) elapsedTime / (float) 1000;
						om.velocity = om.velocity.add(om.acceleration.scale(timeScaleFactor));
						om.position = om.position.add(om.velocity.scale(timeScaleFactor));

						// Opportunity for custom movement code
						om.move(elapsedTime);

						// Look for collisions with walls and/or objects leaving the
						// game-field. If the object should be destroyed due to wall
						// behavior, this method returns true
						if (collideWall(om)) {
							i.remove();
						}
					}
				}
			}

			// Look for collisions and generate events - all possible pairs (simple
			// but inefficient)

			// TODO: Increase efficiency by maintaining separate lists of mobile and
			// immobile objects;
			// still need combined list of all objects for repainting

			// TODO: We current assume that all mobile objects are billiard balls, and
			// all immobile
			// objects are rectangular blocks. We could add an attribute to
			// AnimationObject to state
			// which way to treat each object.

			synchronized (gameObjects) {
				for (int i = 0; i < gameObjects.size() - 1; i++) {
					AnimationObject o1 = gameObjects.get(i);
					for (int j = i + 1; j < gameObjects.size(); j++) {
						AnimationObject o2 = gameObjects.get(j);

						boolean collided;

						if ( (o1 instanceof AnimObjectMobile)
								&& (o2 instanceof AnimObjectMobile)) {
							AnimObjectMobile om1 = (AnimObjectMobile) o1;
							AnimObjectMobile om2 = (AnimObjectMobile) o2;
							// Detect collision of mobile objects using billiard-ball model
							collided = collideMobileObjects(om1, om2);
						} else if (!(o1 instanceof AnimObjectMobile)
								&& !(o2 instanceof AnimObjectMobile)) {
							// nothing to do - cannot collide two immobile objects
							collided = false;
						} else {
							// One mobile and one immobile; first parameter is the mobile one
							if (o1 instanceof AnimObjectMobile) {
								collided = collideWithImmobileObject((AnimObjectMobile) o1, o2);
							} else {
								collided = collideWithImmobileObject((AnimObjectMobile) o2, o1);
							}
						}

						if (collided) {
							o1.fireCollisionEvent(new CollisionEvent(o2));
							o2.fireCollisionEvent(new CollisionEvent(o1));
						}
					}
				}
			}

			// Paint
			repaint();

			long tempTime = (new Date()).getTime();
			long timeSoFar = tempTime - thisTime;

			// We try not to let our framerate exceed 50 FPS
			if (timeSoFar < 20) {
				try {
					Thread.sleep(20 - timeSoFar);
				} catch (InterruptedException e) {
				}
			}
		}
		displayThread = null; // dereference the thread - not really necessary, but
		// a nicety
	}

	/**
	 * This method handles collisions with walls. If an object should be destroyed
	 * by a collision, this method returns true, so that the parent iterator's
	 * remove method can be used. We cannot remove the object ourselves, since our
	 * parent is in the middle of iterating.
	 * 
	 * @param om
	 * @return
	 */
	private boolean collideWall(AnimObjectMobile om) {
		boolean destroy = false;
		Vector position = om.position;
		Vector velocity = om.velocity;
		float width = om.width;
		float height = om.height;

		if (position.x < 0) {
			switch (West_behavior) {
			case Bounce:
				position.x = 0 - position.x;
				velocity.x = -velocity.x;
				break;
			case Destroy:
				if (position.x < 0 - width)
					destroy = true;
				break;
			case Wrap:
				if (position.x < 0 - width)
					position.x += xMax + width;

				break;
			}
		}

		if ((position.x + width) > xMax) {
			switch (East_behavior) {
			case Bounce:
				position.x = 2 * xMax - (position.x + 2 * width);
				velocity.x = -velocity.x;
				break;
			case Destroy:
				if (position.x > xMax)
					destroy = true;
				break;
			case Wrap:
				if (position.x > xMax)
					position.x -= (xMax + width);

				break;
			}
		}

		if (position.y < 0) {
			switch (North_behavior) {
			case Bounce:
				position.y = 0 - position.y;
				velocity.y = -velocity.y;
				break;
			case Destroy:
				if (position.y < 0 - width)
					destroy = true;
				break;
			case Wrap:
				if (position.y < 0 - width)
					position.y += yMax + height;

				break;
			}
		}

		if ((position.y + height) > yMax) {
			switch (South_behavior) {
			case Bounce:
				position.y = 2 * yMax - (position.y + 2 * height);
				velocity.y = -velocity.y;
				break;
			case Destroy:
				if (position.y > yMax)
					destroy = true;
				break;
			case Wrap:
				if (position.y > yMax)
					position.y -= (yMax + height);

				break;
			}
		}

		return destroy;
	}

	private boolean collideWithImmobileObject(AnimObjectMobile o1,
			AnimationObject o2) {
		float overlap;

		boolean collided = false;
		AnimationObject.Coordinates c1 = o1.getCoordinates();
		AnimationObject.Coordinates c2 = o2.getCoordinates();
		if (c1.overlap(c2)) {
			// bounce left or right?
			if (c1.center_y > c2.top & c1.center_y < c2.bottom) {
				if (c1.left < c2.left & c1.right > c2.left) {
					if (o1.bounce) {
						overlap = c1.right - c2.left;
						o1.position.x -= 2 * overlap;
						o1.velocity.x = -o1.velocity.x;
					}
					collided = true;
				} else if (c1.left < c2.right & c1.right > c2.right) {
					if (o1.bounce) {
						overlap = c2.right - c1.left;
						o1.position.x += 2 * overlap;
						o1.velocity.x = -o1.velocity.x;
					}
					collided = true;
				}
			} else if (c1.center_x > c2.left & c1.center_x < c2.right) {
				// bounce up or down?
				if (c1.top < c2.top & c1.bottom > c2.top) {
					if (o1.bounce) {
						overlap = c1.bottom - c2.top;
						o1.position.y -= 2 * overlap;
						o1.velocity.y = -o1.velocity.y;
					}
					collided = true;
				} else if (c1.top < c2.bottom & c1.bottom > c2.bottom) {
					if (o1.bounce) {
						overlap = c2.bottom - c1.top;
						o1.position.y += 2 * overlap;
						o1.velocity.y = -o1.velocity.y;
					}
					collided = true;
				}
			} else {
				// bounce off corner - use billiard-ball algorithm to determine angle of
				// bounce, while maintaining the same absolute velocity. Mobile object
				// is the first ball. The second ball
				// is symmetrically opposite the corner of the block.

				// Determine the coordinates of the corner
				float corner_x, corner_y;
				if (c1.center_x < c2.left) {
					corner_x = c2.left;
				} else {
					corner_x = c2.right;
				}
				if (c1.center_y < c2.top) {
					corner_y = c2.top;
				} else {
					corner_y = c2.bottom;
				}

				// Determine the coordinates of the virtual billiard-ball
				float virtualBall_center_x = 2 * corner_x - c1.center_x;
				float virtualBall_center_y = 2 * corner_y - c1.center_y;
				int virtualBall_x = (int) (virtualBall_center_x - o1.width / 2);
				int virtualBall_y = (int) (virtualBall_center_y - o1.height / 2);

				// Create the virtual billiard ball, with infinite mass
				AnimObjectMobile virtualBall = new SimpleBall("virtual", o1.width,
						o1.height, virtualBall_x, virtualBall_y, 100 * o1.mass, false,
						Color.black);

				// Call the utility function to return the
				collided = collideMobileObjects(o1, virtualBall);
			}
		}
		return collided;
	}

	private boolean collideMobileObjects(AnimObjectMobile o1, AnimObjectMobile o2) {
		boolean collided = false;
		Vector p1 = o1.position;
		Vector p2 = o2.position;
		float sumOfRadii = o1.radius + o2.radius;
		Vector normalVector = p1.subtract(p2);
		if (normalVector.norm() < sumOfRadii) {
			collided = true;

			if (o1.bounce | o2.bounce) {
				// Bounce along normal vector; this is only approximate, since it does
				// not account for mass or velocity
				float bounceDistance = (sumOfRadii - normalVector.norm()) / 2;

				// Find normal and tangential components of v1/v2
				Vector normalUnitVector = normalVector.unitVector();
				Vector tangentalUnitVector = new Vector(-normalUnitVector.y,
						normalUnitVector.x);

				// Quick access to the masses
				float m1 = o1.mass;
				float m2 = o2.mass;

				// Velocity components for both bodies
				Vector v1 = o1.velocity;
				float v1n = normalUnitVector.dot(v1);
				float v1t = tangentalUnitVector.dot(v1);
				
				Vector v2 = o2.velocity;
				float v2n = normalUnitVector.dot(v2);
				float v2t = tangentalUnitVector.dot(v2);

				Vector temp1, temp2;
				// Calculate the updates for the two objects
				if (o1.bounce){ 
					p1.add(normalUnitVector.scale(bounceDistance));
					float v1nNew = (v1n * (m1 - m2) + 2f * m2 * v2n) / (m1 + m2);
					temp1 = (new Vector(normalUnitVector)).scale(v1nNew);
					temp2 = (new Vector(tangentalUnitVector)).scale(v1t);
					v1.set(temp1.add(temp2));
				}

				if (o2.bounce) {
					p2.add(normalUnitVector.scale(-bounceDistance));
					float v2nNew = (v2n * (m2 - m1) + 2f * m1 * v1n) / (m1 + m2);
					temp1 = (new Vector(normalUnitVector)).scale(v2nNew);
					temp2 = (new Vector(tangentalUnitVector)).scale(v2t);
					v2.set(temp1.add(temp2));
				}
			}
		}
		return collided;
	}
	
	/**
	 * Test whether an area on the screen is free, so that a new object can be created without overlapping any existing objects
	 * 
	 * @return true if no objects are in the given space
	 * @param left - the left edge of the area
	 * right - the right edge of the area
	 * top - the top of the area
	 * bottom - the bottom of the area
	 */
	public boolean spaceFree(int left, int top, int right, int bottom) {
		boolean isFree = true;
		AnimationObject.Coordinates cArea = new AnimationObject.Coordinates(left, top, right, bottom);
		
		synchronized(gameObjects) {
			for (Iterator<AnimationObject> i = gameObjects.iterator(); i.hasNext() & isFree;) {
				AnimationObject o = i.next();
				AnimationObject.Coordinates cObject = o.getCoordinates();
				if (cObject.overlap(cArea)) {
					isFree = false;
				}
			}
		}
		return isFree;
	}

	public void addObject(AnimationObject o) {
		synchronized (gameObjects) {
			gameObjects.add(o);
			o.setParent(this);
		}
	}

	public void removeObject(AnimationObject o) {
		synchronized (gameObjects) {
			gameObjects.remove(o);
		}
	}

	public ArrayList<AnimationObject> getGameObjects() {
		return gameObjects;
	}

	protected enum WallBehavior {
		Bounce, Destroy, Wrap
	}
}
