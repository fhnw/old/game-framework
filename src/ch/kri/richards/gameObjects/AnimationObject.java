package ch.kri.richards.gameObjects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.event.EventListenerList;

public abstract class AnimationObject {
	protected GameTemplate parent;
	protected String name;
	protected int width;
	protected int height;
	protected Vector position; // for accuracy, position is maintained internally as a float
	protected Color color;
	protected Image image;

	public AnimationObject(String name, int width, int height, int x_pos, int y_pos, Color c) {
		initObject(name, width, height, x_pos, y_pos);
		this.color = c;
		this.image = null;
	}

	public AnimationObject(String name, int width, int height, int x_pos, int y_pos, String imageName) {
		initObject(name, width, height, x_pos, y_pos);
		this.color = Color.black;

		URL url = getClass().getResource(imageName);
		Image i = Toolkit.getDefaultToolkit().getImage(url);
		this.image = i;
	}
	
	private void initObject(String name, int width, int height, int x_pos, int y_pos) {
		// Initialize attributes
		this.name = name;
		this.width = width;
		this.height = height;
		this.position = new Vector(x_pos, y_pos);
		
		// Register for our own collision events
		this.addCollisionListener(new CollisionListener() {
			@Override
			public void collisionEvent(CollisionEvent e) {
				collision((AnimationObject) e.getSource());
			}
	});
	}
	
	protected void collision(AnimationObject o) {}
	
	// Called each frame, to allow objects to take some action
	public void doEachFrame() {}

	public void paint(Graphics g) {
		if (image != null) {
			g.drawImage(image, (int) position.x, (int) position.y, width, height, parent);
		} else {
			g.setColor(color);
			g.drawRect((int) position.x, (int) position.y, width, height);
		}
		g.setColor(color);
		g.drawString(name, (int) (position.x + width + 5), (int) (position.y + height));
	}

	// Set the parent of this object (i.e., the applet)
	public void setParent(GameTemplate parent) {
		this.parent = parent;
	}

	// Remove this object from the game (de-register with the parent)
	public void destroy() {
		parent.removeObject(this);
	}

	public String getName() {
		return this.name;
	}

	// Return the full position of this object on the screen
	public Coordinates getCoordinates() {
		Coordinates c = new Coordinates();
		c.left = position.x;
		c.right = c.left + this.width;
		c.center_x = (c.left + c.right) / 2;
		c.top = position.y;
		c.bottom = c.top + this.height;
		c.center_y = (c.top + c.bottom) / 2; 
		return c;
	}

	public static class Coordinates {
		float left;
		float center_x;
		float right;
		float top;
		float center_y;
		float bottom;
		
		public Coordinates() {}
		
		public Coordinates(float left, float top, float right, float bottom) {
			this.left = left;
			this.top = top;
			this.right = right;
			this.bottom = bottom;
			this.center_x = (this.left + this.right) / 2;
			this.center_y = (this.top + this.bottom) / 2;
		}

		public boolean overlap(Coordinates c) {
			boolean overlap = false;
			if (this.left <= c.right & this.right >= c.left) {
				if (this.top <= c.bottom & this.bottom >= c.top) {
					overlap = true;
				}
			}
			return overlap;
		}
		
		public int getX() {
			return (int) left;
		}
		
		public int getY() {
			return (int) top;
		}
	}

	// Collision event management; note that the same ListenerList
	// can be used for other event types as well (see fireCollisionEvent)

	// Create the listener list
	protected EventListenerList listenerList = new EventListenerList();

	// Allows classes to register for CollisionEvents
	public void addCollisionListener(CollisionListener listener) {
		listenerList.add(CollisionListener.class, listener);
	}

	// Allow classes to unregister for CollisionEvents
	public void removeMyEventListener(CollisionListener listener) {
		listenerList.remove(CollisionListener.class, listener);
	}

	// Fire a CollisionEvent
	void fireCollisionEvent(CollisionEvent evt) {
		Object[] listeners = listenerList.getListenerList();
		// Each listener occupies two elements - the first is the listener class
		// and the second is the listener instance
		for (int i = 0; i < listeners.length; i += 2) {
			if (listeners[i] == CollisionListener.class) {
				((CollisionListener) listeners[i + 1]).collisionEvent(evt);
			}
		}
	}
}
