package ch.kri.richards.gameObjects;

import java.awt.Color;
import java.awt.Graphics;

public class SimpleBall extends AnimObjectMobile {
	public SimpleBall(String name, int width, int height, int xPos, int yPos, float mass, boolean bounce, Color c) {
		super(name, width, height, xPos, yPos, mass, bounce, c);
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(color);
		g.drawOval((int) position.x, (int) position.y, width, height);
	}

}
